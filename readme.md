To Build this project run:

./mvnw boost:docker-build -Ddockerhub.password=123456

To Push image on docker hub:

./mvnw boost:docker-push -Ddockerhub.password=123456
