# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# Add Maintainer Info
MAINTAINER Alpesh Bhavsar

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar

# Adding properties file
ARG PROP_FILE=src/main/resources/application.properties
ADD $PROP_FILE application.properties
ARG PROP_FILE=src/main/resources/application-mysql.properties
ADD $PROP_FILE application-mysql.properties


# Add the application's jar to the container
ADD ${JAR_FILE} spring-petclinic.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom -Dspring.config.location=/application.properties","-jar","/spring-petclinic.jar"]
